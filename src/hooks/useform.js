import { useState } from 'react';

function useForm(valoresIniciais) {
  const [values, setValues] = useState(valoresIniciais);

  function setValue(chave, valor) {
    setValues({
      ...values,
      [chave]: valor, /* O uso do colchete transforma 'chave' em uma atributo dinâmico, ou seja,
            ele é alterado com base no que receber Ou seja, se enviarmos um nome pela set value,
            essa função irá alterar o 'chave para o campo que está sendo enviado e o valor sendo o
            conteúdo Colocar Filmes e enviar, sistema vai receber chave: 'valor' => nome: 'Filmes' */
    });
  }

  function handleChange(infosDoEvento) {
    setValue(
      infosDoEvento.target.getAttribute('name'), /* ´propriedade getAttribute para recuperar um
            campo 'name' de um input. Generaliza a função e cria a variável */
      infosDoEvento.target.value,
    );
    // setValue('nome', infosDoEvento.target.value);
  }

  function clearForm() {
    setValue(valoresIniciais);
  }

  return {
    values,
    handleChange,
    clearForm,
  };
}

export default useForm;
