import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import { BrowserRouter, Switch, Route } from 'react-router-dom';
import Home from './pages/Home';

import CadastroVideo from './pages/cadastro/Video';
import CadastroCategoria from './pages/cadastro/Categoria';

ReactDOM.render(

  <BrowserRouter>
    {/* Usar o BrowserRouter vai impedir que a página fique se recarregando na troca de funcionalidades, ou seja, a aplicação irá fazer um fake route e apresentar o componente */}
    <Switch>
      <Route path="/cadastro/video" component={CadastroVideo} />
      <Route path="/cadastro/categoria" component={CadastroCategoria} />
      <Route path="/" component={Home} exact />
      <Route component={() => (<div>Página 404</div>)} />

    </Switch>

  </BrowserRouter>,

  document.getElementById('root'),
);
