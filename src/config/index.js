export const URL_BACKEND_TOP = window.location.hostname.includes('localhost')
  ? 'http://localhost:8080'
  : 'https://matlearnflix.herokuapp.com'; // recupera e URL com o json das infos

export default {
  URL_BACKEND_TOP,
};
