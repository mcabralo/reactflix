import React from 'react';
import styled, { css } from 'styled-components';
import Menu from '../Menu';
import Footer from '../Footer';

const Main = styled.main`
    background-color: var(--black);
    color: var(--white);
    flex: 1;
    padding-top: 50px;
    padding-left: 5%;
    padding-right: 5%;
    ${({ paddingAll }) => css`
        padding: ${paddingAll};
    `}
    /* Esses 5% ajudam na visualização, fica dinâmico em relação ao tamanho da tela */
`;

function PageDefault({ children, paddingAll }) {
  // outra maneira de passar o objeto sem usar o link seria deixar (props) e chamar o objeto como {props.children}

  return (
  // Todo componente precisa estar envolto numa div
    <>
      {/* Retira-se a div, e ela se tranforma em um framento, para facilitar a visualização do cod (sintaxe enxuga) */}
      {/* Outra forma é usando o React.Fragment */}
      <Menu />
        <Main paddingAll={paddingAll}>
            {children}
        </Main>
      <Footer />
    </>
  );
}

export default PageDefault;
