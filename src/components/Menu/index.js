import React from 'react';
import { Link } from 'react-router-dom'
import Logo from '../../assets/img/Logo.png';
import './Menu.css';
import Button from '../Button'
// import ButtonLink from './components/ButtonLink';

function Menu() {
    return (
        <nav className="Menu">
            <Link to="/">
                <img className="Logo" src={Logo} alt="MatFlix" />
            </Link>

            <Button as={ Link } className="ButtonLink" to="/cadastro/video">
                Novo Vídeo
            </Button>

        </nav>
        // A grande mudança entre o código acime e este comentado é o uso do router dom, para evitar a atualização do browser. 
        // Além de necessitar o import do react-router-dom, também é trocado o 'a' para 'Link'; 'href' para 'to'
        //<nav className="Menu">
        //     <a href="/">
        //         <img className="Logo" src={Logo} alt="MatFlix" />
        //     </a>

        //     <Button as="a" className="ButtonLink" href="/cadastro/video">
        // Dica** Se algum dia quisermos usa ro 'Link' dentro de um button, entra no syled component e troca o .a para 'const Buttono(link)'
        // e depois faz-se o import
        //         Novo Vídeo
        //     </Button>

        // </nav>

        
    );
}

export default Menu;