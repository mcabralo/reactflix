import React, { useEffect, useState } from 'react';
// import Menu from '../../components/Menu';
// import dadosIniciais from '../../data/dados_iniciais.json';
import BannerMain from '../../components/BannerMain';
import Carousel from '../../components/Carousel';
import PageDefault from '../../components/PageDefault';
// import Footer from '../../components/Footer';
import categoriasRepository from '../../repositories/categorias';

function Home() {
  const [dadosIniciais, setDadosIniciais] = useState([]);

  useEffect(() => {
    categoriasRepository.getAllWithVideos()
      .then((categoriasComVideos) => { // quando os dados forem recuperados
        setDadosIniciais(categoriasComVideos); // mostra os dados
      })
      .catch((err) => {
        console.log(err.message);
      });
  }, []);

  return (
    <PageDefault paddingAll={0}>

      {dadosIniciais.lenght === 0 && (<div>Loading...</div>) }

      {/* {JSON.stringify(dadosIniciais)} Mostrar string json na tela */}

      {dadosIniciais.map((categoria, indice) => {
        if (indice === 0) {
          return (
            <div key={categoria.id}>
              <BannerMain
                videoTitle={dadosIniciais[0].videos[0].titulo}
                url={dadosIniciais[0].videos[0].url}
                videoDescription={dadosIniciais[0].videos[0].description}
              />
              <Carousel
                ignoreFirstVideo
                category={dadosIniciais[0]}
              />
            </div>
          );
        }

        return (
          <Carousel
            key={categoria.id}
            category={categoria}
          />
        );
      })}

      {/* <BannerMain
      videoTitle={dadosIniciais.categorias[0].videos[0].titulo}
      url={dadosIniciais.categorias[0].videos[0].url}
      videoDescription="Quais os maiores benefícios de aprender um novo idioma? Lena demonstra como a nossa visão de,
      mundo é alterada quando nos aventuramos neste univero. E você? Que tal desenvolver uma nova visão de mundo?"
    />

    <Carousel
      category={dadosIniciais.categorias[2]}
    />

    <Carousel
      category={dadosIniciais.categorias[3]}
    />

    <Carousel
      category={dadosIniciais.categorias[4]}
    />

    <Carousel
      category={dadosIniciais.categorias[5]}
    />

    <Carousel
      category={dadosIniciais.categorias[6]}
    /> */}
    </PageDefault>
  );
}

export default Home;
