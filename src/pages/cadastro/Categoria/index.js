import React, { useState, useEffect } from 'react';
import { Link } from 'react-router-dom';
import PageDefault from '../../../components/PageDefault';
import FormField from '../../../components/FormField';
import Button from '../../../components/Button';
import useForm from '../../../hooks/useform';

function CadastroCategoria() {
  const valoresIniciais = {
    nome: '',
    descricao: '',
    cor: '',
  };
  const { handleChange, values, clearForm } = useForm(valoresIniciais);
  const [categorias, setCategorias] = useState([]);

  /* const [nomeDaCategoria, setNomeDaCategoria] = useState('valoresIniciais'); -> o useState
  permite receber o objeto direto dentro do react, o useState age como uma variável que facilita
  a mudança de estado e de valor. Ao colocar o colchete antes do sinal de igual nós conseguimos
  quebrar o state e gerar uma função (que já é incorporada nele mesmo) */


  useEffect(() => {
    const URL_TOP = window.location.hostname.includes('localhost')
      ? 'http://localhost:8080/categorias'
      : 'https://matlearnflix.herokuapp.com/categorias'; // recupera e URL com o json das infos

    fetch(URL_TOP)
      .then(async (respostaDoServidor) => { // sincroniza com o servidor
        const resposta = await respostaDoServidor.json(); // dá o tempo de processamento
        setCategorias([
          ...resposta, // devolve o array dentro de um objeto
        ]);
      });
  }, []);

  return (
    <PageDefault>
      <h1>
        Cadastro de Categoria:
        {values.nome}
      </h1>

      <form onSubmit={function handleSubmit(infosDoEvento) {
        infosDoEvento.preventDefault();
        setCategorias([
          ...categorias,
          /* Esses '...' servem para recuperar todos os valores que compõem o array e transportar
          para esta lista. "Pega tudo e não sobrescreve en apaga" */
          values,
        ]);
        clearForm();
      }}
      >
        <div>

          <FormField
            label="Nome da Categoria"
            type="text"
            name="nome"
            value={values.nome}
            onChange={handleChange}
          />

          <FormField
            label="Descrição"
            type="textarea"
            name="descricao"
            value={values.descricao}
            onChange={handleChange}
          />
          {/* <label>
                        Descrição:
                    <textarea type="text" value={values.descricao}
                            name="descricao"
                            onChange={handleChange}
                        />
                    </label> */}

          <FormField
            label="Cor"
            type="color"
            name="cor"
            value={values.cor}
            onChange={handleChange}
          />
          {/* <label>
                        Cor:
                            <input type="color" value={values.cor}
                            name="cor "
                            onChange={handleChange}
                        />
                    </label> */}

        </div>
        <Button>
          Cadastrar
        </Button>
      </form>

      <ul>
        {categorias.map((categoria) => (
        // usar o índice é uma solução também elegante para diminuir os erros do console
          <li key={`${categoria.titulo}`}>
            {categoria.titulo}
          </li>
        ))}
      </ul>

      <Link to="/">
        Ir para a Home
      </Link>
    </PageDefault>

  );
}

export default CadastroCategoria;
